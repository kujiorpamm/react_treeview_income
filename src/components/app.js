import React, {Component} from 'react';
import axios from 'axios';
import up from './../images/up.png';
import down from './../images/down.png';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            category: [],
            childs: [],
            path: [],
            years: [],
            cities: [],
            config: this.props.config,
            current_city: 1,
            current_year: 2017,
            current_node: 0,
            mult: 1
        };
        this.setChild = this.setChild.bind(this);
        this.setCity = this.setCity.bind(this);
        this.setYear = this.setYear.bind(this);
        this.setMult = this.setMult.bind(this);
    }

    componentDidMount() {
        axios.get(this.state.config.url, {
            params: {
                city: this.state.current_city,
                year: this.state.current_year
        	},
        })
          .then(res => {
            this.setState({
                isLoading: false,
                category: res.data.parent,
                childs: res.data.childs,
                path: res.data.path,
                years: res.data.years,
                cities: res.data.cities
            });
          })
    }

    space(number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    get_data()  {
        axios.get(this.state.config.url, {
            params: {
                node: this.state.current_node,
                city: this.state.current_city,
                year: this.state.current_year
            }
        })
        .then(res => {
            console.log(res.data);
            if(res.data.status == true && res.data.childs.length > 0) {
                this.setState({
                    isLoading: false,
                    category: res.data.parent,
                    childs: res.data.childs,
                    path: res.data.path
                });
            } else if(res.data.status == false) {
                console.log('123');
                this.setState({
                    isLoading: false,
                    category: [],
                    childs: []
                });
            }

        })
    }

    setCity(e) {
        this.setState({
            current_city: e.target.value,
            node: 0
        }, () => this.get_data());
    }

    setYear(e) {
        // console.log('123');
        this.setState({
            current_year: e.target.value,
            node: 0
        }, () => this.get_data());
    }

    setMult(e) {
        this.setState({
            mult: e.target.value
        })
    }

    setChild(e) {
        // console.log(e.target.id);

        this.setState({
            isLoading: true
        });

        axios.get(this.state.config.url, {
            params: {
                node: e.target.getAttribute('data-id'),
                city: this.state.current_city,
                year: this.state.current_year
            }
        })
        .then(res => {

            if(res.data.status == true && res.data.childs.length > 0) {
                this.setState({
                    isLoading: false,
                    category: res.data.parent,
                    childs: res.data.childs,
                    path: res.data.path
                });
            } else {
                this.setState({
                    isLoading: false
                });
            }

        })
    }

    render() {

        let childs = this.state.childs.map((item) => {
            // return <div  key={Math.random()}> {item.name} </div>;
            let diff_main = (item.budget_corrected - item.budget_execution).toFixed(1);
            let diff = (item.budget_execution - item.budget_prev_execution).toFixed(1);
            let diff_percent = ((diff * 100) / item.budget_execution).toFixed(1);
            return <tr key={Math.random()}>
                <td> <div data-id={item.id} onClick={this.setChild} key={Math.random()}> {item.name} </div> </td>
                <td> {this.space( (item.budget_corrected * this.state.mult).toFixed(1) )} </td>
                <td> {this.space( (item.budget_execution * this.state.mult).toFixed(1) )} </td>
                <td> {this.space( (diff_main * this.state.mult).toFixed(2) )} </td>
                <td> {item.budget_execution_percent_total} </td>
                <td> {this.space( (item.budget_prev_corrected * this.state.mult).toFixed(2))} </td>
                <td> {this.space((item.budget_prev_execution  * this.state.mult).toFixed(2))} </td>
                <td> {this.space((diff  * this.state.mult).toFixed(2))} </td>
                <td> {diff_percent} <img width='12px' src={ (diff_percent > 0) ? up : down } /> </td>
            </tr>
        });

        let path = this.state.path.map((item) => {
            return <span data-id={item.id} onClick={this.setChild} key={Math.random()}> {item.name} </span>;
        });

        let years = this.state.years.map((item) => {
            return <option value={item}  key={Math.random()}> {item} </option>;
        });
        let cities = this.state.cities.map((item) => {
            return <option value={item.id}  key={Math.random()}> {item.name_ru} </option>;
        });

        return (
            <div className="treeview-income">
                <div className="loading-overlay" style={{display: (this.state.isLoading)? 'block' : 'none' }}></div>
                <div className="row current_item_heading">
                    <div className="col-xs-9"><div className="current_item">{path}</div></div>
                    <div className="col-xs-3">
                        <form className="form-inline">
                            <div className="form-group">
                                <select onChange={this.setCity} id="wgt_city" value={this.state.current_city} className="form-control" >
                                    {cities}
                                </select>
                                <select onChange={this.setYear} id="wgt_year" value={this.state.current_year} className="form-control" >
                                    {years}
                                </select>
                                <select onChange={this.setMult} id="wgt_mult" value={this.state.mult} className="form-control">
                                    <option value="1">тыс. тг</option>
                                    <option value="0.001">млн. тг</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <table className="table table-dark">
                    <thead>
                        <tr>
                            <th>Наименование платежа</th>
                            <th>Годовой план</th>
                            <th>Годовое исполнение</th>
                            <th>Не исполнено</th>
                            <th>% Исполнения</th>
                            <th>Аналогичный период прошлого года</th>
                            <th>Исполнение аналогичного периода</th>
                            <th>Рост</th>
                            <th>Рост, %</th>
                        </tr>
                    </thead>
                    <tbody>
                        {childs}
                    </tbody>
                </table>
            </div>
        );
    }

}
